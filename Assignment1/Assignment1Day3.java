package Assignment1;
import java.util.Arrays;
import java.util.Scanner;


public class Assignment1Day3 {
    public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            System.out.print("Panjang array: ");
            int panjangArray = input.nextInt();
            int[] arrayInput = new int[panjangArray];

            for (int i = 0; i < panjangArray; i++) {
                System.out.print("index [" + i + "] : ");
                arrayInput[i] = input.nextInt();
            }
            System.out.println("Panjang Array = " + panjangArray);
            System.out.println("Array integer = " + Arrays.toString(arrayInput));
            }
    }
