package Assignment4;
import Assignment4.Mobil;

import java.util.*;

public class Assignment4Day3 {
    public static void main(String[] args) {
//Creating user-defined class objects
        Mobil s1 = new Mobil("Honda", "Silver", 2020);
        Mobil s2 = new Mobil("BMW", "Hitam", 2022);
        Mobil s3 = new Mobil("Lamborghini", "Kuning", 2005);

        ArrayList<Mobil> al = new ArrayList<Mobil>();
        al.add(s1);//adding Student class object
        al.add(s2);
        al.add(s3);
        Iterator itr = al.iterator();

//traversing elements of ArrayList object
        while (itr.hasNext()) {
            Mobil st = (Mobil) itr.next();
            System.out.println(st.merek + " " + st.warna + " " + st.tahunPembuatan);
        }
    }
}






