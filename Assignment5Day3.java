package Day3;
import java.util.*;

public class Assignment5Day3 {

        public static void main(String args[]){
            ArrayList<String> nama= new ArrayList<String>();
            nama.add("Peter");
            nama.add("John");
            nama.add("Billy");
            System.out.println("Array list name:" + nama);


            nama.add("Jack");
            System.out.println("Array list name after add:"+ nama);

            nama.remove("Peter");
            System.out.println("Array list name after delete:"+ nama);

            ArrayList<String> retainList= new ArrayList<String>();
            retainList.add("John");
            retainList.add("Jack");
            nama.retainAll(retainList);
            System.out.println("Array list name after retain:"+ nama);
        }
    }

