package Assignment3;
import java.util.Arrays;
import java.util.Scanner;

public class Assignment3Day3{

    public static int[] bubbleSort(int[] a){
//        int[] a = {4, 85, 7, 1, 0, 36, -5, 48};

        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1 - i; j++) {

                if (a[j + 1] < a[j]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;


                }
            }
        }
        return a;
    }
    static void binarySearch(int[] a, int target) {
        int left = 0;
        int right = a.length - 1;
        int middle;
        while (left <= right) {
            middle = (left+right) / 2;
            if(a[middle] == target) {
                System.out.println("Element " + target + " found at index "+middle);
                break;
            } else if (a[middle]< target){
                left = middle + 1;
            } else if (a[middle] > target) {
                right = middle - 1;
            }
        }
        if ( left > right ){
            System.out.println("Element " + target + " is not found!");
        }
    }
    public static boolean isSorted(int[] a)
    {
        // base case
        if (a == null || a.length <= 1) {
            return true;
        }

        for (int i = 0; i < a.length - 1; i++)
        {
            if (a[i] > a[i + 1]) {
                return false;
            }
        }

        return true;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int userInput;
        int array[] = {};

        do {
            System.out.println("Array");
            System.out.println("1. Input data Array");
            System.out.println("2. Sortir data Array");
            System.out.println("3. Search data Array");
            System.out.println("4. EXIT");


            System.out.println("Input Nomor ");
            userInput = input.nextInt();
            if (userInput == 4) {
                break;
            } else if (userInput == 1) {
                System.out.print("Baris array: ");
                int size = input.nextInt();
                array = new int[size]; //Nentuin panjang array
                for (int i = 0; i < size; i++) {
                    System.out.print("index [" + i + "] : ");
                    array[i] = input.nextInt(); //Nama array
                }
                System.out.println(Arrays.toString(array));
            }
            else if (userInput == 2) {
                if (array.length == 0) {
                    System.out.println("Array invalid");
                } else {
                    int[] aSorted = bubbleSort(array);

                    for (int el : aSorted) {
                        System.out.print(el + " ");
                    }
                    System.out.println();
                }
            }
            else if (userInput == 3) {
                if (array.length ==0 ) {
                    System.out.println("Jumlah array tidak boleh 0");
                }
                else {
                    System.out.println("Masukkan Target : ");
                    int target = input.nextInt();
                    binarySearch(array, target);
                }
            }

        } while (userInput !=4);
    }
}