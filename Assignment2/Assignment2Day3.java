package Assignment2;
import java.util.Arrays;
import java.util.Scanner;



public class Assignment2Day3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Baris array: ");
        int baris = input.nextInt();
        System.out.print("Kolom array: ");
        int kolom = input.nextInt();
        int[][] array = new int[baris][kolom];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; ++j) {
                System.out.println("[" + i + "][" + j + "]: ");
                array[i][j] = input.nextInt();
            }
        }
        System.out.println("Baris dan Kolom Array 2 Dimensi adalah [" + baris + "][" + kolom + "]");

        System.out.print("Array 2 Dimensi\n");
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; ++j) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}